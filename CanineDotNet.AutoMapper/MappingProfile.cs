﻿using AutoMapper;
using CanineDotNet.AutoMapper.Interfaces;
using System;
using System.Linq;
using System.Reflection;

namespace CanineDotNet.AutoMapper
{ 
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var mapFromTypes = assembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>)))
                .ToList();

            var mapToTypes = assembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapTo<>)))
                .ToList();
            foreach (var type in mapFromTypes)
            {
                var instance = Activator.CreateInstance(type);
                var methodInfo = type.GetMethod("Mapping");
                if (methodInfo == null)
                    methodInfo = type.GetInterface(typeof(IMapFrom<object>).Name).GetMethod("Mapping");
                methodInfo?.Invoke(instance, new object[] { this });
            }
            foreach (var type in mapToTypes)
            {
                var instance = Activator.CreateInstance(type);
                var methodInfo = type.GetMethod("Mapping");
                if (methodInfo == null)
                    methodInfo = type.GetInterface(typeof(IMapTo<object>).Name).GetMethod("Mapping");
                methodInfo?.Invoke(instance, new object[] { this });
            }
        }
    }
}
