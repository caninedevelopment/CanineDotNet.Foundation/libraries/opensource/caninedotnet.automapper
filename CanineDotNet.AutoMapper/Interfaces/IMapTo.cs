﻿using AutoMapper;

namespace CanineDotNet.AutoMapper.Interfaces
{
    public interface IMapTo<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(GetType(), typeof(T));
    }
}
