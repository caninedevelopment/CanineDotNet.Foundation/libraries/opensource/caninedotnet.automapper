﻿using AutoMapper;

namespace CanineDotNet.AutoMapper.Interfaces
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
